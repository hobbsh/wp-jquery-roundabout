=== Plugin Name ===
Contributors: hobbsh
Donate link: http://wyliehobbs.com/
Tags: jquery roundabout, roundabout slider, jquery slider, carousel slider
Requires at least: 3.0
Tested up to: 3.4.1
Stable tag: 1.0
License: BSD-2

This plugin utilizes the jQuery Roundabout plugin by Fred LeBlanc to output your WordPress posts or post attachments in a revolving style slider.

== Description ==

This plugin utilizes the jQuery Roundabout plugin by Fred LeBlanc to output your WordPress posts or post attachments in a revolving style slider.


== Installation ==

1. Unzip `jquery-roundabout-for-wordpress-posts.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use shortcode [wprabt-slider] in page or template to display slider.
4. See usage page for more options

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==


= 1.0 =
Initial Release - more options to come


== A brief Markdown Example ==

Use shortcode [wprabt-slider] in page content area to display slider. Use settings page to change options.